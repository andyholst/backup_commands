#!/usr/bin/env bash

##############################################
# Sync git repositories from current directory
##############################################

ls -d */ | xargs -I{} bash -c "sed -i 's/url = git@github.com:/url = git@gitlab.com:/g' {}/.git/config"

